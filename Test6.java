
class Vehicle {
    private int numberOfWheels;
    protected boolean mesinPembakaran;

    public Vehicle() { // Default constructor
        this.numberOfWheels = 2;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void display() {
        System.out.println("Number of Wheels: " + getNumberOfWheels());
    }
}

class Bicycle extends Vehicle {

    public Bicycle() {
        this.mesinPembakaran = false;
    }

    public boolean mesinPembakaran() {
        return mesinPembakaran;
    }

    @Override
    public void display() {
        System.out.println("Number of Wheels: " + getNumberOfWheels());
        System.out.println("Motorcycle punya mesin pembakaran: " + mesinPembakaran());
    }
}

class Motorcycle extends Vehicle {

    public Motorcycle() {
        this.mesinPembakaran = true;
    }

    public boolean mesinPembakaran() {
        return mesinPembakaran;
    }

    @Override
    public void display() {
        System.out.println("Number of Wheels: " + getNumberOfWheels());
        System.out.println("Motorcycle punya mesin pembakaran: " + mesinPembakaran());
    }
}

public class Test6 {
    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle();
        Motorcycle motorcycle = new Motorcycle();
        bicycle.display();
        motorcycle.display();
    }
}
