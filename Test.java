public class Test { //
    // Metode main membuat sebuah instansi dari kelas Test dan memanggil metode
    // start-nya.
    public static void main(String[] args) {
        Test obj = new Test();
        obj.start();
    }

    public void start() {
        // Metode start menginisialisasi variabel string stra dengan nilai "do" dan
        // memanggil metode method dengan stra sebagai argumen. Kemudian, mencetak nilai
        // yang dihasilkan dari penggabungan stra dan nilai yang dikembalikan dari
        // method.
        String stra = "do";
        String strb = method(stra);
        System.out.print(": " + stra + strb);
    }

    public String method(String stra) {
        // Metode method mengambil argumen string stra, menambahkan "good" kepadanya,
        // mencetak string hasilnya, dan mengembalikan string " good".
        stra = stra + "good";
        System.out.print(stra);
        return " good";
    }
}

/*
 * Output: do good
 */