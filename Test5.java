class Test5 {
    public static void main(String[] args) {
        int arr[] = { 1, 2, 3 }; // inisiasi array int arr[]

        // final with for-each statement
        for (final int i : arr)
            System.out.print(i + " ");
    }
}

/*
 * Output: 1 2 3
 */