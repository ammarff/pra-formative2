public class Test2 {
    int a = 1;
    int b = 2;

    // func: Metode ini mengambil sebuah objek dari tipe Test2 sebagai parameter dan
    // mengembalikan sebuah objek dari tipe Test2. Di dalam metode ini, ia membuat
    // sebuah objek baru obj3 dari tipe Test2, menugaskan obj ke obj3, dan melakukan
    // beberapa operasi pada variabel instan a dan b dari objek-objek tersebut.
    // Akhirnya, ia mengembalikan obj3.
    public Test2 func(Test2 obj) {
        Test2 obj3 = new Test2();
        obj3 = obj;
        obj3.a = obj.a++ + ++obj.b;
        obj.b = obj.b;
        return obj3;
    }

    // main: Ini adalah titik masuk program. Metode ini membuat sebuah objek obj1
    // dari tipe Test2, memanggil metode func dengan obj1 sebagai argumen, dan
    // menugaskan objek yang dikembalikan ke obj2. Kemudian, ia mencetak nilai dari
    // variabel instan a dan b untuk kedua obj1 dan obj2.
    public static void main(String[] args) {
        Test2 obj1 = new Test2();
        Test2 obj2 = obj1.func(obj1);

        System.out.println("obj1.a = " + obj1.a + " obj1.b = " + obj1.b);
        System.out.println("obj2.a = " + obj2.a + " obj1.b = " + obj2.b);
        System.out.println("obj2 = " + obj2);
        System.out.println("obj1 = " + obj1);

    }
}
